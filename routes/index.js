var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/view', function(req, res, next) {
  data = {"courseName" : "Certified Ethereum Developer", "certificateID" : "KBACED1001", "cadndidateName" : "Ananthan", "grade" : "S", "date" : "2021-02-20"}
  res.render('viewCertificate', {data: data});
});

router.post('/view', function(req, res, next) {
  data = req.body;
  console.log(data);
  res.send(data)
  // res.render('viewCertificate');
});

router.get('/issue', function(req, res, next) {
  res.render('issueCertificate', {formClass : '', messageClass : 'hidden', certificateID : ''});
});

router.post('/issue', function(req, res, next) {
  data = req.body;
  console.log(data);
  res.send(data)
  // res.render('issueCertificate', {formClass : 'hidden', messageClass : '', certificateID : ''});
});

module.exports = router;
